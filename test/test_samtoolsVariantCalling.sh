#!/usr/bin/sh

nextflow run /home/ngp001/workflows/samtoolsVariantCalling/main.nf \
    -c /home/ngp001/workflows/samtoolsVariantCalling/nf.conf \
    --samplesheet /home/ngp001/workflows/samtoolsVariantCalling/test/samplesheet.csv \
    --ref /home/ngp001/workflows/references/ecoli/ecoli-rel606.fa \
    --idx_path /home/ngp001/workflows/references/ecoli/ \
    --outputDir .