# samtools-variant-calling

## <ins>**Summary**
This repo contains a simple example of a nextflow workflow. In this workflow, variants are called using a common variant calling pipeline using samtools and bcftools. This workflow was developed on a debian system with 4GB of RAM. 

## <ins>**Usage**
<details><summary>Dependencies</summary>

* Nextflow executable
* Docker

</details>

<details><summary>Running a Workflow</summary>
To run a workflow, one can use the following syntax:

```sh
nextflow run main.nf \
    -c <file.config> \                  # A file containing nextflow configurations
    --samplesheet <samples.csv> \       # File containing the samples
    --ref <path_to/ref.fa> \            # The path to the reference fasta file
    --idx_path <path_to/bwa_idx> \      # The path to the bwa references
    --outputDir <output_directory_path> # Path to output directory
```
</details>
<br/>

## <ins>**Input File Examples**
<details><summary>samples.csv</summary>

The workflow processes the reads for both single-end and paired-end. 
```
sampleId,read1,read2
SRR2584857,/home/nickproject/Desktop/projects/repos/junk-genomics/testData/ecoli_samtoolsVariantCalling/SRR2584857.fq.gz
```

</details>
<br/>


## <ins>**Workflow Steps**
**Step 1: BWA Alignment**
<details><summary>Summary</summary>

The reads are first aligned using the BWA-mem aligner with default parameters. In the same command, reads are converted to BAM using `samtools view`.
</details>

<details><summary>Outputs</summary>

* BAM file containing the aligned reads
</details>
<br/>

**Step 2: Variant Calling using bcftools**
<details><summary>Summary</summary>

Using the BAM file and reference *.fa, the genotype likelihoods are called using are called using `bcftools mpileup` and the variants are called using `bcftools call`. 
</details>

<details><summary>Outputs</summary>

* A vcf file containing variant calls
</details>
<br/>