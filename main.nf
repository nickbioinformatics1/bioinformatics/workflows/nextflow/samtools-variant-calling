#!/home/ngp001/bins/nextflow

nextflow.enable.dsl=2

// Define params for workflow
params.samplesheet=''
params.ref=''
params.idx_path=''
params.outputDir=''

// Log info about workflow
log.info """\
    BWA-SAMTOOLS-VARIANTCALLING PIPELINE
    ==========================
    Samplesheet:               ${params.samplesheet}
    Reference Used:            ${params.ref}
    Index Path:                ${params.idx_path}
    Output Directory:          ${params.outputDir}
    """

process bwaAlign {
    publishDir "${params.outputDir}/alignment", mode: 'copy'
    
    container 'quay.io/refgenomics/docker-bwa'

    input:
        path(idx_path)
        val(sampleId)
        path(reads)
    
    output:
        tuple val("${sampleId}"), path("${sampleId}_sorted.bam"), emit: bamFile_ch
            

    script:
        idx_base = idx_path[0].baseName
        
        """
        bwa mem -t 1 "${idx_base}" ${reads} | samtools view -bS - > ${sampleId}_sorted.bam
        """
}

process callVariants {
    publishDir "${params.outputDir}/variantCalls", mode: 'copy'
    
    container 'pegi3s/samtools_bcftools'

    input:
        path(ref_file)
        tuple val(sampleId),path(bamFile)

    output:
        path "${sampleId}_variants.raw.bcf"
    
    script:
        """
        bcftools mpileup -E -f ${ref_file} ${bamFile} | bcftools call -mv -Ob -o ${sampleId}_variants.raw.bcf
        """
}

workflow {
    // --------- CHECKING COMMAND LINE ARGUMENTS -----------
    // Check for output dir
    if (!params.outputDir) {
        log.error("Please specify an output directory")
        System.exit(1)
    }

    // Check for reference file
    if(!params.ref) {
        log.error("The reference .fa was not provided")
        System.exit(1)
    } else {
        fa_ref = Channel.fromPath(params.ref)
    }

    // Check for Path to indexes
    if(!params.idx_path) {
        log.error("The path to indexes was not provided")
        System.exit(1)
    } else {
       idx_files = Channel.fromPath(params.idx_path + '/*').collect()
    }
    
    // Process input CSV
    Channel
        .fromPath(params.samplesheet)
        .splitCsv( header: true, sep: ',' )
        .multiMap { row -> 
            sampleId: row.sampleId
            reads: [row.read1,row.read2].findAll {it != null} // remove null values. A Method to detect single or pair endedness
        }
        .set { reads_ch }

    // Run bwa align process
    bwaAlign(idx_files,reads_ch.sampleId, reads_ch.reads)

    // Call Variants
    callVariants(fa_ref,bwaAlign.out.bamFile_ch)
}




